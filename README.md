# Object Detection with YOLO

This algorithm uses a pre-trained YOLO v2 model to detect the obstacles on the road ahead every few seconds as the driver drives around.

## Requirements

`pip install pandas`

`pip install tensorflow`

## Results

Original Road Video (takes a moment to load):

![Original](/original.gif "Original")

Annotated Result Video (takes a moment to load):

![Annotated](/annotated.gif "Annotated")


